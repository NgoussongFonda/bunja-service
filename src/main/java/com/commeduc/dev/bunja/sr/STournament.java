/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.commeduc.dev.bunja.sr;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

/**
 *
 * @author deugueu
 */
public class STournament implements Serializable {
    
    @SerializedName("id")
    private String id;
    @SerializedName("name")
    private String name;

    @SerializedName("sport")
    private SSport sport;

    @SerializedName("category")
    private SCategory category;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SSport getSport() {
        return sport;
    }

    public void setSport(SSport sport) {
        this.sport = sport;
    }

    public SCategory getCategory() {
        return category;
    }

    public void setCategory(SCategory category) {
        this.category = category;
    }
    
    public int getJustSrId(){
        int index = ("sr:tournament:").length();
        return Integer.parseInt( getId().substring( index ) );
    }
    
}
