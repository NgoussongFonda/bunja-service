/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.commeduc.dev.bunja.sr;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fonda
 */
public class LineUpResponse implements Serializable {
    
    @SerializedName("lineups")
    private List<SLineUp> lineups = new ArrayList();
    
    @SerializedName("sport_event")
    private SSportEvent sport_event;

    public SSportEvent getSport_event() {
        return sport_event;
    }

    public void setSport_event(SSportEvent sport_event) {
        this.sport_event = sport_event;
    }
    
    public List<SLineUp> getLineups() {
        return lineups;
    }

    public void setLineups(List<SLineUp> lineups) {
        this.lineups = lineups;
    }    
    
}
