/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.commeduc.dev.bunja.sr;


import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 *
 * @author deugueu
 */

public interface RepositoryInterface {
    
    @GET("/{sportId}/{zoneId}/{langId}/{serviceId}/{date}/schedule.json")
    Call<ScheduleResponse> getSchedule(
            @Path(value="sportId") String sportId,
            @Path(value="zoneId") String zoneId,
            @Path(value="langId") String langId,
            @Path(value="serviceId") String serviceId,
            @Path(value="date") String date,
            @Query("api_key") String api_key
    );
    
    @GET("/soccer-t3/{zoneId}/{langId}/{serviceId}/{matchId}/lineups.json")
    Call<LineUpResponse> getLineUp(
            @Path(value="zoneId") String zoneId,
            @Path(value="langId") String langId,
            @Path(value="serviceId") String serviceId,
            @Path(value="matchId") String matchId,
            @Query("api_key") String api_key
    );
  
}