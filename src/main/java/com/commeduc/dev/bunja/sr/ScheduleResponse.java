/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.commeduc.dev.bunja.sr;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author deugueu
 */
public class ScheduleResponse implements Serializable {
    
    
    @SerializedName("generated_at")
    private String generated_at;

    @SerializedName("sport_events")
    private List<SSportEvent> sport_events = new ArrayList();

    public String getGenerated_at() {
        return generated_at;
    }

    public void setGenerated_at(String generated_at) {
        this.generated_at = generated_at;
    }

    public List<SSportEvent> getSport_events() {
        return sport_events;
    }

    public void setSport_events(List<SSportEvent> sport_events) {
        this.sport_events = sport_events;
    }
    
    
}
