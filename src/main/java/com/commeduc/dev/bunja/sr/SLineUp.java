/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.commeduc.dev.bunja.sr;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fonda
 */
public class SLineUp implements Serializable{
    
    @SerializedName("team")
    public String team;
    @SerializedName("formation")
    public String formation;
    
    @SerializedName("manager")
    private SManager manager;
    
    @SerializedName("starting_lineup")
    private List<SStartingLineup> starting_lineup = new ArrayList();
    
    @SerializedName("substitutes")
    private List<SSubstitutes> substitutes = new ArrayList();
    
    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public String getFormation() {
        return formation;
    }

    public void setFormation(String formation) {
        this.formation = formation;
    }

    public SManager getManager() {
        return manager;
    }

    public void setManager(SManager manager) {
        this.manager = manager;
    }

    public List<SStartingLineup> getStarting_lineup() {
        return starting_lineup;
    }

    public void setStarting_lineup(List<SStartingLineup> starting_lineup) {
        this.starting_lineup = starting_lineup;
    }

    public List<SSubstitutes> getSubstitutes() {
        return substitutes;
    }

    public void setSubstitutes(List<SSubstitutes> substitutes) {
        this.substitutes = substitutes;
    } 
    
}
