/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.commeduc.dev.bunja.sr;

import org.springframework.stereotype.Service;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
import java.util.List;
import org.springframework.web.bind.annotation.PathVariable;
/**
 *
 * @author deugueu
 */
@Service
public class SportRadarService implements APIConfiguration  {
    
    
    private String accessToken;
    private RepositoryInterface service;

    
    public SportRadarService() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        service = retrofit.create(RepositoryInterface.class);
    }

    
    // https://api.sportradar.us/soccer-t3/eu/en/schedules/2019-01-20/schedule.json?api_key=yv4z9y3n75xrhva2p95smd7h
    public ScheduleResponse getSchedules( String sportId, String zoneId, String langId, String serviceId, String date) throws IOException {
        
        //String sportId = "soccer-t3", zoneId = "eu", langId  = "en", serviceId = "schedules", date = "2019-01-20";
        
        Call<ScheduleResponse> retrofitCall = service.getSchedule(sportId, zoneId, langId, serviceId, date, API_KEY);
        Response<ScheduleResponse> response = retrofitCall.execute();

        if (!response.isSuccessful()) {
            throw new IOException(response.errorBody() != null
                    ? response.errorBody().string() : "Unknown error");
        }

        return response.body();
    }

    // https://api.sportradar.us/soccer-t3/eu/en/matches/sr:match:16594213/lineups.json?api_key=yv4z9y3n75xrhva2p95smd7h
    public LineUpResponse getLineUps( String zoneId, String langId, String serviceId, String matchId) throws IOException {
        
        //String sportId = "soccer-t3", zoneId = "eu", langId  = "en", serviceId = "matches", matchId = "sr:match:16594213";
        
        Call<LineUpResponse> retrofitCall = service.getLineUp(zoneId, langId, serviceId, matchId, API_KEY);
        Response<LineUpResponse> response = retrofitCall.execute();

        if (!response.isSuccessful()) {
            throw new IOException(response.errorBody() != null
                    ? response.errorBody().string() : "Unknown error LineUp");
        }

        return response.body();
    }



    
    
}
