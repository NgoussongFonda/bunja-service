/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.commeduc.dev.bunja.sr;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author deugueu
 */
public class SSportEvent  implements Serializable {
    
    @SerializedName("id")
    public String id;
    @SerializedName("scheduled")
    public String scheduled;
    @SerializedName("status")
    public String status;

    @SerializedName("competitors")
    private List<SCompetitors> competitors = new ArrayList();

    @SerializedName("season")
    private SSeason season;

    @SerializedName("tournament")
    private STournament tournament;

    @SerializedName("tournament_round")
    private STournamentRound tournament_round;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getScheduled() {
        return scheduled;
    }

    public void setScheduled(String scheduled) {
        this.scheduled = scheduled;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<SCompetitors> getCompetitors() {
        return competitors;
    }

    public void setCompetitors(List<SCompetitors> competitors) {
        this.competitors = competitors;
    }

    public SSeason getSeason() {
        return season;
    }

    public void setSeason(SSeason season) {
        this.season = season;
    }

    public STournament getTournament() {
        return tournament;
    }

    public void setTournament(STournament tournament) {
        this.tournament = tournament;
    }

    public STournamentRound getTournament_round() {
        return tournament_round;
    }

    public void setTournament_round(STournamentRound tournament_round) {
        this.tournament_round = tournament_round;
    }
    
    public int getJustSrId(){
        int index = ("sr:match:").length();
        return Integer.parseInt( getId().substring( index ) );
    }
    
    public Date getJustSchedule(){
        //DateFormat.parse(getScheduled());
        DateFormat format = new SimpleDateFormat();
        System.out.println( " HOSIRUS >>>>> "+getSeason().getName() + " >>> "+ getScheduled() );

        ZonedDateTime zonedDateTime = ZonedDateTime.parse(getScheduled()); 
        return Date.from(zonedDateTime.toInstant());
        
    }
    
}
