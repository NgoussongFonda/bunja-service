/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.commeduc.dev.bunja.sr;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

/**
 *
 * @author deugueu
 */
public class STournamentRound  implements Serializable {
    
    @SerializedName("type")
    private String type;
    @SerializedName("number")
    private int number;
    @SerializedName("phase")
    private String phase;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getPhase() {
        return phase;
    }

    public void setPhase(String phase) {
        this.phase = phase;
    }
    
}
