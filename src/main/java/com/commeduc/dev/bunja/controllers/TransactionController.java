/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.commeduc.dev.bunja.controllers;

import com.commeduc.dev.bunja.models.MatchsDao;
import com.commeduc.dev.bunja.models.TransactionTypeDao;
import com.commeduc.dev.bunja.models.TransactionsDao;
import com.commeduc.dev.bunja.models.UserDao;
import com.commeduc.dev.bunja.oo.BetEvent;
import com.commeduc.dev.bunja.oo.BetType;
import com.commeduc.dev.bunja.oo.Matchs;
import com.commeduc.dev.bunja.oo.TransactionType;
import com.commeduc.dev.bunja.oo.Transactions;
import com.commeduc.dev.bunja.oo.User;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author fonda
 */

@Controller
public class TransactionController {
    
   @Autowired
   private TransactionTypeDao transactionTypeDao;
   @Autowired
   private UserDao userDao;
   @Autowired
   private TransactionsDao transactionsDao;
   
   
   @RequestMapping(value="/bunja/services/transactions/{transactionTypeId}/{userId}/{amount}/{account}/{password}", method = {RequestMethod.GET},
            /*consumes = MediaType.APPLICATION_XML_VALUE,*/
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE) 
   @ResponseBody
    public Transactions transactionService(
            
            @PathVariable(value="transactionTypeId") int transactionTypeId,
            @PathVariable(value="userId") int userId,
            @PathVariable("amount") String amount,
            @PathVariable(value="account") String account,
            @PathVariable(value="password") String password ) {
        
        Transactions response = null;
        
        if( response == null){
            
            try {
                
                Transactions transactions = new Transactions();
                Date now = new Date();
                transactions.setTimestamp(now);
                transactions.setAmount(amount);

                TransactionType transactionType = transactionTypeDao.getTransactionTypeById(transactionTypeId);
                transactions.setTransactionTypeId(transactionType);

                User user = userDao.getUserById(userId);
                transactions.setUserId(user);

                return transactionsDao.create(transactions);
                
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        
        return response;
    } 
}
