package com.commeduc.dev.bunja.controllers;

import com.commeduc.dev.bunja.Utils;
import com.commeduc.dev.bunja.models.UserDao;
import com.commeduc.dev.bunja.oo.City;
import com.commeduc.dev.bunja.oo.Country;
import com.commeduc.dev.bunja.oo.User;
import java.util.Calendar;
import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Class UserController
 */
@Controller
public class UserController {

  // ------------------------
  // PUBLIC METHODS
  // ------------------------

    
     /**
   * Create a new user and returns .
     * @param username
     * @param msisdn
     * @param password
     * @param email
     * @return 
   */
  @RequestMapping(value="/user/register/{msisdn}/{email}/{password}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  @ResponseBody
  public User tryQuickRegistration( 
          @PathVariable("password") String password,
          @PathVariable("msisdn") String msisdn, 
          @PathVariable("email") String email) {
              
    try {
        
      User user = userDao.getUserByMsIsdn(msisdn);
          User errorUser = new User(-1000);
          errorUser.setAddress( "User already exists with "+ msisdn );
          return errorUser;
      
    }
    catch(NoResultException e){
        
        User nuser = new User();
          nuser.setFirstName("UNKNOWN");
          nuser.setLastName("UNKNOWN");
          nuser.setEmail(email);
          nuser.setPassword(Utils.md5(password));
          
          City city = new City(1);
          nuser.setCityId(city);
          
          nuser.setAddress("UNKNOWN");
          nuser.setPhoneNumber(msisdn);
          nuser.setMsisdn(msisdn);
          
          Country country = new Country(37);
          nuser.setCountryId(country);
          
          nuser.setCreationDate( Calendar.getInstance().getTime() );
          
          System.out.println(" ++++++++++++++++ "+ nuser.toString() );
          // Insert user in database
          return userDao.create(nuser);
      
    }
    catch (Exception ex) {
        ex.printStackTrace();
    }
    return null;
  }    
    
     /**
   * Retrieve the id for the card with the passed code.
     * @param msisdn
     * @param password
     * @return 
   */
  @RequestMapping(value="/user/login/{msisdn}/{password}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  @ResponseBody
  public User tryLogin(@PathVariable("msisdn") String msisdn, @PathVariable("password") String password) {
              
    try {
      User user = userDao.getUserByMsIsdn(msisdn);
      if(user.getPassword().equals( Utils.md5(password)) ){
          return user;
      }
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
    return null;
  }
  
     /**
   * Retrieve the id for the card with the passed code.
     * @param msisdn
     * @return 
   */
  @RequestMapping(value="/user/autologin/{msisdn}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  @ResponseBody
  public User autoLogin(@PathVariable("msisdn") String msisdn) {
              
    try {
      User user = userDao.getUserByMsIsdn(msisdn);
      if(user.getMsisdn().equals(msisdn) ){
          return user;
      }
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
    return null;
  }

    /**
   * Retrieve the id for the card with the passed code.
     * @param msisdn
     * @return 
   */
  @RequestMapping(value="/user/get/msisdn", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  @ResponseBody
  public User getByMsIsdn(String msisdn) {
    try {
      User card = userDao.getUserByMsIsdn(msisdn);
      return card;
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
    return null;
  }
  
     /**
   * Update new user and returns .
     * @param msisdn 
     * @param firstName
     * @param lastName
     * @param address
     * @return 
   */
  @RequestMapping(value="/user/update/{msisdn}/{firstName}/{lastName}/{address}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  @ResponseBody
  public User tryUpdate( 
          @PathVariable("msisdn") String msisdn,
          @PathVariable("firstName") String firstName,
          @PathVariable("lastName") String lastName, 
          @PathVariable("address") String address){
              
    try {
       User user = userDao.getUserByMsIsdn(msisdn);
       user.setFirstName(firstName);
       user.setLastName(lastName);
       user.setAddress(address);
       
       userDao.update(user);
       return user;
         
    }
    catch(NoResultException e){
        
        User nuser = new User();
        nuser.setId(-1000);
        nuser.setAddress("Update failed : No user found with "+msisdn );
        return nuser;
    }
    catch (Exception ex) {
        ex.printStackTrace();
    }
    return null;
  }  
  
  /**
   * Retrieve the id for the card with the passed code.
     * @param email
     * @return 
   */
  @RequestMapping(value="/user/get/mail", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  @ResponseBody
  public User getByCode(String email) {
      
    try {
      User card = userDao.getUserByEmail(email);
      return card;
    }
    catch (Exception ex) {
        ex.printStackTrace();
    }
    
    return null;
  }
  
   /**
   * Retrieve the id for the card with the passed code.
     * @param searchQuery
     * @return 
   */
  @RequestMapping(value="/user/get/{searchQuery}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  @ResponseBody
  public List<User> getBySearch(
          @PathVariable("searchQuery") String searchQuery) {
    try {
      List<User> card = userDao.getUserSearch(searchQuery);
      return card;
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
    return null;
  }

  // ------------------------
  // PRIVATE FIELDS
  // ------------------------
  
  // Wire the UserDao used inside this controller.
  @Autowired
  private UserDao userDao;
  
} // class UserController
