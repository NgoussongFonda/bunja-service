/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.commeduc.dev.bunja.controllers;

import com.commeduc.dev.bunja.models.BetBattleChoiceDao;
import com.commeduc.dev.bunja.models.BetBattleConditionDao;
import com.commeduc.dev.bunja.models.BetBattleDao;
import com.commeduc.dev.bunja.models.BetBattleParticipantsDao;
import com.commeduc.dev.bunja.models.BetEventDao;
import com.commeduc.dev.bunja.models.BetTypeDao;
import com.commeduc.dev.bunja.models.ConditionsDao;
import com.commeduc.dev.bunja.models.MatchsDao;
import com.commeduc.dev.bunja.models.UserDao;
import com.commeduc.dev.bunja.oo.BetBattle;
import com.commeduc.dev.bunja.oo.BetBattleChoice;
import com.commeduc.dev.bunja.oo.BetBattleCondition;
import com.commeduc.dev.bunja.oo.BetBattleParticipants;
import com.commeduc.dev.bunja.oo.BetEvent;
import com.commeduc.dev.bunja.oo.BetType;
import com.commeduc.dev.bunja.oo.Conditions;
import com.commeduc.dev.bunja.oo.Matchs;
import com.commeduc.dev.bunja.oo.User;
import java.io.IOException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 *
 * @author fonda
 */
@Controller
public class BetEventsController {
    
    @Autowired
    private MatchsDao matchsDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private BetTypeDao betTypeDao;
    @Autowired
    private BetEventDao betEventDao;
    @Autowired
    private BetBattleDao betBattleDao;
    @Autowired
    private BetBattleParticipantsDao betBattleParticipantsDao;
    @Autowired
    private ConditionsDao conditionsDao;
    @Autowired
    private BetBattleConditionDao betBattleconditionsDao;
    @Autowired
    private BetBattleChoiceDao betBattleChoiceDao;
    
   @RequestMapping(value="/bunja/services/bet_event/{name}/{matchId}/{betTypeId}/{userId}/{account}/{password}", method = {RequestMethod.GET},
            /*consumes = MediaType.APPLICATION_XML_VALUE,*/
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE) 
   @ResponseBody
    public BetEvent betEventService(
            
            @PathVariable(value="name") String name,
            @PathVariable(value="matchId") int matchId,
            @PathVariable(value="betTypeId") int betTypeId,
            @PathVariable("userId") int userId,
            @PathVariable(value="account") String account,
            @PathVariable(value="password") String password ) {
       
        BetEvent response = null;
        if (response == null){
            try {
                BetEvent betevent = new BetEvent();
                betevent.setName(name);

                Matchs match = matchsDao.getMatchsBySrId(matchId);
                betevent.setMatchId(match);

                BetType betType = betTypeDao.getBetTypeById(betTypeId);
                betevent.setBetTypeId(betType);

                User user = userDao.getUserById(userId);
                betevent.setUserId(user);

                Date now = new Date();
                betevent.setTimestamp(now);

                System.out.print(">>>>>>>>>>>>>>>>>>>>>>>>>>>"+match);
                System.out.print(">>>>>>>>>>>>>>>>>>>>>>>>>>>"+betType);
                System.out.print(">>>>>>>>>>>>>>>>>>>>>>>>>>>"+user);         

                return betEventDao.create(betevent);
        
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        
        return response;
            
    } 
    
    @RequestMapping(value="/bunja/services/bet_battle/{validate}/{beteventId}/{account}/{password}", method = {RequestMethod.GET},
            /*consumes = MediaType.APPLICATION_XML_VALUE,*/
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE) 
    @ResponseBody
    public BetBattle betBattleService(
            
            @PathVariable(value="validate") boolean validate,
            @PathVariable(value="beteventId") int beteventId,
            @PathVariable(value="account") String account,
            @PathVariable(value="password") String password ) {
        
        BetBattle response = null;
        
        if (response == null){
            
            try {
                
                BetBattle betbattle = new BetBattle();
                betbattle.setValidated(validate);
                Date now = new Date();
                betbattle.setTimestamp(now);
                
                BetEvent betEvent = betEventDao.getBetEventById(beteventId);
                System.out.println("HERE I AM>>>>>>>>>>"
                        +beteventId+ ">>>>"+betEvent);
                betbattle.setBetEventId(betEvent);

                BetBattle bb = betBattleDao.create(betbattle);

                /*
                BetBattleParticipants betBattleParticipants = new BetBattleParticipants();
                User user = userDao.getUserById(userId);
                betBattleParticipants.setUserId(user);

                betBattleParticipants.setBetBattleId(bb);
                betBattleParticipants.setInitiator(true);
                betBattleParticipants.setSide(true);

                BetBattleParticipants bbp = betBattleParticipantsDao.findBetBattleParticipants(bb, user);
                if(bbp == null){
                    bbp = betBattleParticipantsDao.create(betBattleParticipants);
                }
                


                BetBattleCondition betBattleCondition = new BetBattleCondition();
                //betBattleCondition.setBetBattleId(bb);

                Conditions condition = conditionsDao.getConditionsById(conditionId);
                betBattleCondition.setConditionId(condition);
                System.out.print(">>>>>>>>>>>>>>>>>>>>>>"+condition);
                betBattleCondition.setOccurence(1);
                betBattleCondition.setBetBattleId(bb);

                BetBattleCondition bbc = betBattleconditionsDao.findBetBattleCondition(bb, condition);
                if(bbc == null){
                    bbc = betBattleconditionsDao.create(betBattleCondition);
                }
                */

                return bb;      
        
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        
        return response;
        
    }
    
    @RequestMapping(value="/bunja/services/bet_battle_participants/{side}/{initiator}/{betBattleId}/{userId}/{account}/{password}", method = {RequestMethod.GET},
            /*consumes = MediaType.APPLICATION_XML_VALUE,*/
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE) 
    @ResponseBody
    public BetBattleParticipants betBattleParticipantsService(
            
            @PathVariable(value="side") int side,
            @PathVariable(value="initiator") int initiator,
            @PathVariable(value="betBattleId") int betBattleId,
            @PathVariable("userId") int userId,
            @PathVariable(value="account") String account,
            @PathVariable(value="password") String password ) {
        
        BetBattleParticipants response = null;
        
        if (response == null) {
            
            try {
                
                BetBattleParticipants betBattleParticipants = new BetBattleParticipants();
                betBattleParticipants.setSide(true);
                betBattleParticipants.setInitiator(true);
                
                User user = userDao.getUserById(userId);
                betBattleParticipants.setUserId(user);
                
                BetBattle betBattle = betBattleDao.getBetBattleById(betBattleId);
                betBattleParticipants.setBetBattleId(betBattle);
                
                BetBattleParticipants bbp = betBattleParticipantsDao.create(betBattleParticipants);
                
                return bbp;
                
                
            }catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        
        return response;
    }
    
    @RequestMapping(value="/bunja/services/bet_battle_condition/{occurence}/{value}/{betBattleId}/{conditionId}/{account}/{password}", method = {RequestMethod.GET},
            /*consumes = MediaType.APPLICATION_XML_VALUE,*/
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE) 
    @ResponseBody
    public BetBattleCondition betBattleConditionService (
            
            @PathVariable(value="occurence") int occurence,
            @PathVariable(value="value") int value,
            @PathVariable(value="betBattleId") int betBattleId,
            @PathVariable("conditionId") int conditionId,
            @PathVariable(value="account") String account,
            @PathVariable(value="password") String password) {
        
        BetBattleCondition response = null;
        
        if (response == null) {
            
            try {
                
                BetBattleCondition betBattleCondition = new BetBattleCondition();
                betBattleCondition.setOccurence(occurence);
                betBattleCondition.setValue(value);
                
                BetBattle betBattle = betBattleDao.getBetBattleById(betBattleId);
                betBattleCondition.setBetBattleId(betBattle);
                
                Conditions conditions = conditionsDao.getConditionsById(conditionId);
                betBattleCondition.setConditionId(conditions);
                
                BetBattleCondition bbc = betBattleconditionsDao.create(betBattleCondition);
                
                return bbc;
                
            }catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        
        return response;
        
    }
    
    @RequestMapping(value="/bunja/services/bet_battle_choice/{choice}/{betBattleConditionId}/{betBattleParticipantsId}/{betBattleId}/{account}/{password}", method = {RequestMethod.GET},
            /*consumes = MediaType.APPLICATION_XML_VALUE,*/
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE) 
    @ResponseBody
    public BetBattleChoice betBattleChoiceService (
        
            @PathVariable(value="choice") String choice,
            @PathVariable(value="betBattleConditionId") int betBattleConditionId,
            @PathVariable(value="betBattleParticipantsId") int betBattleParticipantsId,
            @PathVariable("betBattleId") int betBattleId,
            @PathVariable(value="account") String account,
            @PathVariable(value="password") String password) {
        
        BetBattleChoice response = null;
        
        if (response == null) {
            
            try {
                
                BetBattleChoice betBattleChoice = new BetBattleChoice();
                betBattleChoice.setChoice(choice);
                
                BetBattleCondition betBattleCondition = betBattleconditionsDao.getBetBattleConditionById(betBattleConditionId);
                betBattleChoice.setBetBattleConditionId(betBattleCondition);
                
                BetBattleParticipants betBattleParticipants = betBattleParticipantsDao.getBetBattleById(betBattleParticipantsId);
                betBattleChoice.setBetBattleParticipantsId(betBattleParticipants);
                
                BetBattle betBattle = betBattleDao.getBetBattleById(betBattleId);
                betBattleChoice.setBetBattleId(betBattle);
                
                BetBattleChoice bbc = betBattleChoiceDao.create(betBattleChoice);
                
                return bbc;
                
            }catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        
        return response;
    }  
    
    
}

