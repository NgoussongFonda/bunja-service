package com.commeduc.dev.bunja.controllers;

import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class MainController {

  @RequestMapping("/")
  @ResponseBody
  public String index() {
    return "Proudly handcrafted by " +
        "<a href='https://www.commeduc.com/'>COMMEDUC @237</a>  "+ Calendar.getInstance().get(Calendar.YEAR) +" :)";
  }

}
