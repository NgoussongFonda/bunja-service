/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.commeduc.dev.bunja.oo;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author fonda
 */
@Entity
@Table(name = "tournament_team")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TournamentTeam.findAll", query = "SELECT t FROM TournamentTeam t"),
    @NamedQuery(name = "TournamentTeam.findById", query = "SELECT t FROM TournamentTeam t WHERE t.id = :id")})
public class TournamentTeam implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "homeTeamId")
    @JsonManagedReference
    private Collection<Matchs> matchsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "visitingTeamId")
    @JsonManagedReference
    private Collection<Matchs> matchsCollection1;
    @JoinColumn(name = "team_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    @JsonBackReference
    private Team teamId;
    @JoinColumn(name = "tournament_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    @JsonBackReference
    private Tournament tournamentId;

    public TournamentTeam() {
    }

    public TournamentTeam(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @XmlTransient
    public Collection<Matchs> getMatchsCollection() {
        return matchsCollection;
    }

    public void setMatchsCollection(Collection<Matchs> matchsCollection) {
        this.matchsCollection = matchsCollection;
    }

    @XmlTransient
    public Collection<Matchs> getMatchsCollection1() {
        return matchsCollection1;
    }

    public void setMatchsCollection1(Collection<Matchs> matchsCollection1) {
        this.matchsCollection1 = matchsCollection1;
    }

    public Team getTeamId() {
        return teamId;
    }

    public void setTeamId(Team teamId) {
        this.teamId = teamId;
    }

    public Tournament getTournamentId() {
        return tournamentId;
    }

    public void setTournamentId(Tournament tournamentId) {
        this.tournamentId = tournamentId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TournamentTeam)) {
            return false;
        }
        TournamentTeam other = (TournamentTeam) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.commeduc.dev.bunja.oo.TournamentTeam[ id=" + id + " ]";
    }
    
}
