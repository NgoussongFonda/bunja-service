/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.commeduc.dev.bunja.oo;

import com.fasterxml.jackson.annotation.JsonBackReference;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fonda
 */
@Entity
@Table(name = "bet_condition")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BetCondition.findAll", query = "SELECT b FROM BetCondition b"),
    @NamedQuery(name = "BetCondition.findById", query = "SELECT b FROM BetCondition b WHERE b.id = :id"),
    @NamedQuery(name = "BetCondition.findByConditionId", query = "SELECT b FROM BetCondition b WHERE b.conditionId = :conditionId"),
    @NamedQuery(name = "BetCondition.findByOccurence", query = "SELECT b FROM BetCondition b WHERE b.occurence = :occurence"),
    @NamedQuery(name = "BetCondition.findByValue", query = "SELECT b FROM BetCondition b WHERE b.value = :value")})
public class BetCondition implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "occurence")
    private int occurence;
    @Column(name = "value")
    private Integer value;
    @JoinColumn(name = "bet_event_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    @JsonBackReference
    private BetEvent betEventId;
    @JoinColumn(name = "condition_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    @JsonBackReference
    private Conditions conditionId;

    public BetCondition() {
    }

    public BetCondition(Integer id) {
        this.id = id;
    }

    public BetCondition(Integer id, int occurence) {
        this.id = id;
        this.occurence = occurence;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getOccurence() {
        return occurence;
    }

    public void setOccurence(int occurence) {
        this.occurence = occurence;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public BetEvent getBetEventId() {
        return betEventId;
    }

    public void setBetEventId(BetEvent betEventId) {
        this.betEventId = betEventId;
    }

    public Conditions getConditionsId() {
        return conditionId;
    }

    public void setConditionsId(Conditions conditionId) {
        this.conditionId = conditionId;
    }
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BetCondition)) {
            return false;
        }
        BetCondition other = (BetCondition) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.commeduc.dev.bunja.oo.BetCondition[ id=" + id + " ]";
    }
    
}
