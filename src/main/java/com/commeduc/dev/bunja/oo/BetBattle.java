/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.commeduc.dev.bunja.oo;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author fonda
 */
@Entity
@Table(name = "bet_battle")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BetBattle.findAll", query = "SELECT b FROM BetBattle b"),
    @NamedQuery(name = "BetBattle.findById", query = "SELECT b FROM BetBattle b WHERE b.id = :id"),
    @NamedQuery(name = "BetBattle.findByMatchId", query = "SELECT b FROM BetBattle b WHERE b.betEventId = :betEventId"),
    @NamedQuery(name = "BetBattle.findByValidated", query = "SELECT b FROM BetBattle b WHERE b.validated = :validated"),
    @NamedQuery(name = "BetBattle.findByTimestamp", query = "SELECT b FROM BetBattle b WHERE b.timestamp = :timestamp")})
public class BetBattle implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "validated")
    private boolean validated;
    @Basic(optional = false)
    @NotNull
    @Column(name = "timestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timestamp;
    @JoinColumn(name = "bet_event_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    @JsonBackReference
    private BetEvent betEventId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "betBattleId")
    @JsonManagedReference
    private Collection<BetBattleParticipants> betBattleParticipantsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "betBattleId")
    @JsonManagedReference
    private Collection<BetBattleCondition> betBattleConditionCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "betBattleId")
    @JsonManagedReference
    private Collection<BetBattleChoice> betBatteChoiceCollection;

    public BetBattle() {
    }

    public BetBattle(Integer id) {
        this.id = id;
    }

    public BetBattle(Integer id, boolean validated, Date timestamp) {
        this.id = id;
        this.validated = validated;
        this.timestamp = timestamp;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean getValidated() {
        return validated;
    }

    public void setValidated(boolean validated) {
        this.validated = validated;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public BetEvent getBetEventId() {
        return betEventId;
    }

    public void setBetEventId(BetEvent betEventId) {
        this.betEventId = betEventId;
    }

    @XmlTransient
    public Collection<BetBattleParticipants> getBetBattleParticipantsCollection() {
        return betBattleParticipantsCollection;
    }

    public void setBetBattleParticipantsCollection(Collection<BetBattleParticipants> betBattleParticipantsCollection) {
        this.betBattleParticipantsCollection = betBattleParticipantsCollection;
    }

    @XmlTransient
    public Collection<BetBattleCondition> getBetBattleConditionCollection() {
        return betBattleConditionCollection;
    }

    public void setBetBattleConditionCollection(Collection<BetBattleCondition> betBattleConditionCollection) {
        this.betBattleConditionCollection = betBattleConditionCollection;
    }

    @XmlTransient
    public Collection<BetBattleChoice> getBetBatteChoiceCollection() {
        return betBatteChoiceCollection;
    }

    public void setBetBatteChoiceCollection(Collection<BetBattleChoice> betBatteChoiceCollection) {
        this.betBatteChoiceCollection = betBatteChoiceCollection;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BetBattle)) {
            return false;
        }
        BetBattle other = (BetBattle) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.commeduc.dev.bunja.oo.BetBattle[ id=" + id + " ]";
    }
    
}
