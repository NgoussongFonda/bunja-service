/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.commeduc.dev.bunja.oo;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author fonda
 */
@Entity
@Table(name = "conditions")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Conditions.findAll", query = "SELECT c FROM Conditions c"),
    @NamedQuery(name = "Conditions.findById", query = "SELECT c FROM Conditions c WHERE c.id = :id"),
    @NamedQuery(name = "Conditions.findByName", query = "SELECT c FROM Conditions c WHERE c.name = :name"),
    @NamedQuery(name = "Conditions.findByAbbreviation", query = "SELECT c FROM Conditions c WHERE c.abbreviation = :abbreviation"),
    @NamedQuery(name = "Conditions.findByParentCondition", query = "SELECT c FROM Conditions c WHERE c.parentCondition = :parentCondition")})
public class Conditions implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 120)
    @Column(name = "name")
    private String name;
    @Size(max = 20)
    @Column(name = "abbreviation")
    private String abbreviation;
    @Column(name = "parent_condition")
    private Integer parentCondition;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "conditionId")
    @JsonManagedReference
    private Collection<BetBattleCondition> betBattleConditionCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "conditionId")
    @JsonManagedReference
    private Collection<BetCondition> betConditionCollection;

    public Conditions() {
    }

    public Conditions(Integer id) {
        this.id = id;
    }

    public Conditions(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public Integer getParentCondition() {
        return parentCondition;
    }

    public void setParentCondition(Integer parentCondition) {
        this.parentCondition = parentCondition;
    }

    @XmlTransient
    public Collection<BetBattleCondition> getBetBattleConditionCollection() {
        return betBattleConditionCollection;
    }

    public void setBetBattleConditionCollection(Collection<BetBattleCondition> betBattleConditionCollection) {
        this.betBattleConditionCollection = betBattleConditionCollection;
    }
    
     @XmlTransient
    public Collection<BetCondition> getBetConditionCollection() {
        return betConditionCollection;
    }

    public void setBetConditionCollection(Collection<BetCondition> betConditionCollection) {
        this.betConditionCollection = betConditionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Conditions)) {
            return false;
        }
        Conditions other = (Conditions) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.commeduc.dev.bunja.oo.Conditions[ id=" + id + " ]";
    }
    
}
