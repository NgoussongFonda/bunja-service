/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.commeduc.dev.bunja.oo;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author fonda
 */
@Entity
@Table(name = "bet_battle_condition")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BetBattleCondition.findAll", query = "SELECT b FROM BetBattleCondition b"),
    @NamedQuery(name = "BetBattleCondition.findById", query = "SELECT b FROM BetBattleCondition b WHERE b.id = :id"),
    @NamedQuery(name = "BetBattleCondition.findByOccurence", query = "SELECT b FROM BetBattleCondition b WHERE b.occurence = :occurence"),
    @NamedQuery(name = "BetBattleCondition.findByValue", query = "SELECT b FROM BetBattleCondition b WHERE b.value = :value")})
public class BetBattleCondition implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "occurence")
    private int occurence;
    @Column(name = "value")
    private Integer value;
    @JoinColumn(name = "bet_battle_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    @JsonBackReference
    private BetBattle betBattleId;
    @JoinColumn(name = "condition_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    @JsonBackReference
    private Conditions conditionId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "betBattleConditionId")
    @JsonManagedReference
    private Collection<BetBattleChoice> betBatteChoiceCollection;

    public BetBattleCondition() {
    }

    public BetBattleCondition(Integer id) {
        this.id = id;
    }

    public BetBattleCondition(Integer id, int occurence) {
        this.id = id;
        this.occurence = occurence;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getOccurence() {
        return occurence;
    }

    public void setOccurence(int occurence) {
        this.occurence = occurence;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public BetBattle getBetBattleId() {
        return betBattleId;
    }

    public void setBetBattleId(BetBattle betBattleId) {
        this.betBattleId = betBattleId;
    }

    public Conditions getConditionId() {
        return conditionId;
    }

    public void setConditionId(Conditions conditionId) {
        this.conditionId = conditionId;
    }

    @XmlTransient
    public Collection<BetBattleChoice> getBetBatteChoiceCollection() {
        return betBatteChoiceCollection;
    }

    public void setBetBatteChoiceCollection(Collection<BetBattleChoice> betBatteChoiceCollection) {
        this.betBatteChoiceCollection = betBatteChoiceCollection;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BetBattleCondition)) {
            return false;
        }
        BetBattleCondition other = (BetBattleCondition) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.commeduc.dev.bunja.oo.BetBattleCondition[ id=" + id + " ]";
    }
    
}
