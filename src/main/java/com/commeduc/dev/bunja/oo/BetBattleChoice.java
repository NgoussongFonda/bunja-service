/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.commeduc.dev.bunja.oo;

import com.fasterxml.jackson.annotation.JsonBackReference;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fonda
 */
@Entity
@Table(name = "bet_battle_choice")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BetBattleChoice.findAll", query = "SELECT b FROM BetBattleChoice b"),
    @NamedQuery(name = "BetBattleChoice.findById", query = "SELECT b FROM BetBattleChoice b WHERE b.id = :id"),
    @NamedQuery(name = "BetBattleChoice.findByChoice", query = "SELECT b FROM BetBattleChoice b WHERE b.choice = :choice")})
public class BetBattleChoice implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "choice")
    private String choice;
    @JoinColumn(name = "bet_battle_condition_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    @JsonBackReference
    private BetBattleCondition betBattleConditionId;
    @JoinColumn(name = "bet_battle_participants_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    @JsonBackReference
    private BetBattleParticipants betBattleParticipantsId;
    @JoinColumn(name = "bet_battle_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    @JsonBackReference
    private BetBattle betBattleId;

    public BetBattleChoice() {
    }

    public BetBattleChoice(Integer id) {
        this.id = id;
    }

    public BetBattleChoice(Integer id, String choice) {
        this.id = id;
        this.choice = choice;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getChoice() {
        return choice;
    }

    public void setChoice(String choice) {
        this.choice = choice;
    }

    public BetBattleCondition getBetBattleConditionId() {
        return betBattleConditionId;
    }

    public void setBetBattleConditionId(BetBattleCondition betBattleConditionId) {
        this.betBattleConditionId = betBattleConditionId;
    }

    public BetBattleParticipants getBetBattleParticipantsId() {
        return betBattleParticipantsId;
    }

    public void setBetBattleParticipantsId(BetBattleParticipants betBattleParticipantsId) {
        this.betBattleParticipantsId = betBattleParticipantsId;
    }

    public BetBattle getBetBattleId() {
        return betBattleId;
    }

    public void setBetBattleId(BetBattle betBattleId) {
        this.betBattleId = betBattleId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BetBattleChoice)) {
            return false;
        }
        BetBattleChoice other = (BetBattleChoice) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.commeduc.dev.bunja.oo.BetBattleChoice[ id=" + id + " ]";
    }
    
}
