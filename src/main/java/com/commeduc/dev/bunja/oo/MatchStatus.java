/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.commeduc.dev.bunja.oo;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author fonda
 */
@Entity
@Table(name = "match_status")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MatchStatus.findAll", query = "SELECT m FROM MatchStatus m"),
    @NamedQuery(name = "MatchStatus.findById", query = "SELECT m FROM MatchStatus m WHERE m.id = :id"),
    @NamedQuery(name = "MatchStatus.findByName", query = "SELECT m FROM MatchStatus m WHERE m.name = :name"),
    @NamedQuery(name = "MatchStatus.findByAbbreviation", query = "SELECT m FROM MatchStatus m WHERE m.abbreviation = :abbreviation")})
public class MatchStatus implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "abbreviation")
    private String abbreviation;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "matchStatusId")
    @JsonManagedReference
    private Collection<Matchs> matchsCollection;

    public MatchStatus() {
    }

    public MatchStatus(Integer id) {
        this.id = id;
    }

    public MatchStatus(Integer id, String name, String abbreviation) {
        this.id = id;
        this.name = name;
        this.abbreviation = abbreviation;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    @XmlTransient
    public Collection<Matchs> getMatchsCollection() {
        return matchsCollection;
    }

    public void setMatchsCollection(Collection<Matchs> matchsCollection) {
        this.matchsCollection = matchsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MatchStatus)) {
            return false;
        }
        MatchStatus other = (MatchStatus) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.commeduc.dev.bunja.oo.MatchStatus[ id=" + id + " ]";
    }
    
}
