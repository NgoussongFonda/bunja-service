package com.commeduc.dev.bunja.models;

import com.commeduc.dev.bunja.oo.BetBattle;
import com.commeduc.dev.bunja.oo.BetBattleParticipants;
import com.commeduc.dev.bunja.oo.Team;
import com.commeduc.dev.bunja.oo.Tournament;
import com.commeduc.dev.bunja.oo.TournamentTeam;
import com.commeduc.dev.bunja.oo.User;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

/**
 * This class is used to access data for the User entity.
 * Repository annotation allows the component scanning support to find and 
 * configure the DAO without any XML configuration and also provide the Spring 
 * exception translation.
 * Since we've setup setPackagesToScan and transaction manager on
 * DatabaseConfig, any bean method annotated with Transactional will cause
 * Spring to magically call begin() and commit() at the start/end of the
 * method. If exception occurs it will also call rollback().
 */
@Repository
@Transactional
public class BetBattleParticipantsDao {
  
  // ------------------------
  // PUBLIC METHODS
  // ------------------------
  
  /**
   * Save the betbattleParticipants in the database.
     * @param betbattleParticipants
   */
  public BetBattleParticipants create(BetBattleParticipants betbattleParticipants) {
    entityManager.persist(betbattleParticipants);
    return betbattleParticipants;
  }
  
  /**
   * Delete the betbattleParticipants from the database.
     * @param betbattleParticipants
   */
  public void delete(BetBattleParticipants betbattleParticipants) {
    if (entityManager.contains(betbattleParticipants))
      entityManager.remove(betbattleParticipants);
    else
      entityManager.remove(entityManager.merge(betbattleParticipants));
    return;
  }
  
  /**
   * Return all the betbattleParticipants stored in the database.
     * @return 
   */
  @SuppressWarnings("unchecked")
  public List<BetBattleParticipants> getAll() {
    return entityManager.createQuery("from BetBattleParticipants").getResultList();
  }
  
  /**
   * Return the betbattleParticipants having the passed id.
     * @param betbattleParticipants
     * @return a betbattleParticipants retrieved from its id
   */
  public BetBattleParticipants getBetBattleById(int id) {
      return entityManager.find(BetBattleParticipants.class, id);
  }
  
  /**
   * Update the passed betbattleParticipants in the database.
     * @param betbattleParticipants
   */
  public void update(BetBattleParticipants betbattleParticipants) {
      entityManager.merge(betbattleParticipants); 
  }
  
   public BetBattleParticipants findBetBattleParticipants(BetBattle betBattleId, User userId) {
      
      try {
        return (BetBattleParticipants)entityManager.createQuery("from BetBattleParticipants WHERE bet_battle_id = :betbattleId AND user_id = :userId")
                .setParameter("betbattleId", betBattleId.getId())
                .setParameter("userId", userId.getId()).getSingleResult();
      }
      catch(Exception e){}
      
      return null;
  }

  // ------------------------
  // PRIVATE FIELDS
  // ------------------------
  
  // An EntityManager will be automatically injected from entityManagerFactory
  // setup on DatabaseConfig class.
  @PersistenceContext
  private EntityManager entityManager;
  
} // class UserDao
