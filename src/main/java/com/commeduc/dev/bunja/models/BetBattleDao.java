package com.commeduc.dev.bunja.models;

import com.commeduc.dev.bunja.oo.BetBattle;
import com.commeduc.dev.bunja.oo.User;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

/**
 * This class is used to access data for the User entity.
 * Repository annotation allows the component scanning support to find and 
 * configure the DAO without any XML configuration and also provide the Spring 
 * exception translation.
 * Since we've setup setPackagesToScan and transaction manager on
 * DatabaseConfig, any bean method annotated with Transactional will cause
 * Spring to magically call begin() and commit() at the start/end of the
 * method. If exception occurs it will also call rollback().
 */
@Repository
@Transactional
public class BetBattleDao {
  
  // ------------------------
  // PUBLIC METHODS
  // ------------------------
  
  /**
   * Save the betbattle in the database.
     * @param betbattle
   */
  public BetBattle create(BetBattle betbattle) {
    entityManager.persist(betbattle);
    return betbattle;
  }
  
  /**
   * Delete the betbattle from the database.
     * @param betbattle
   */
  public void delete(BetBattle betbattle) {
    if (entityManager.contains(betbattle))
      entityManager.remove(betbattle);
    else
      entityManager.remove(entityManager.merge(betbattle));
    return;
  }
  
  /**
   * Return all the betbattle stored in the database.
     * @return 
   */
  @SuppressWarnings("unchecked")
  public List<BetBattle> getAll() {
    return entityManager.createQuery("from BetBattle").getResultList();
  }
  
  /**
   * Return the betbattle having the passed id.
     * @param betbattle
     * @return an BetBattle retrieved from its id
   */
  public BetBattle getBetBattleById(int id) {
      return entityManager.find(BetBattle.class, id);
  }
  
  /**
   * Update the passed betbattle in the database.
     * @param betbattle
   */
  public void update(BetBattle betbattle) {
      entityManager.merge(betbattle); 
  }

  // ------------------------
  // PRIVATE FIELDS
  // ------------------------
  
  // An EntityManager will be automatically injected from entityManagerFactory
  // setup on DatabaseConfig class.
  @PersistenceContext
  private EntityManager entityManager;
  
} // class UserDao
