package com.commeduc.dev.bunja.models;

import com.commeduc.dev.bunja.oo.User;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

/**
 * This class is used to access data for the User entity.
 * Repository annotation allows the component scanning support to find and 
 * configure the DAO without any XML configuration and also provide the Spring 
 * exception translation.
 * Since we've setup setPackagesToScan and transaction manager on
 * DatabaseConfig, any bean method annotated with Transactional will cause
 * Spring to magically call begin() and commit() at the start/end of the
 * method. If exception occurs it will also call rollback().
 */
@Repository
@Transactional
public class UserDao {
  
  // ------------------------
  // PUBLIC METHODS
  // ------------------------
  
  /**
   * Save the user in the database.
     * @param user
   */
  public User create(User user) {
    entityManager.persist(user);
    return user;
  }
  
  /**
   * Delete the user from the database.
     * @param card
   */
  public void delete(User card) {
    if (entityManager.contains(card))
      entityManager.remove(card);
    else
      entityManager.remove(entityManager.merge(card));
    return;
  }
  
  /**
   * Return all the users stored in the database.
     * @return 
   */
  @SuppressWarnings("unchecked")
  public List<User> getAll() {
    return entityManager.createQuery("from User").getResultList();
  }
  
  /**
   * Return the user having the passed email.
     * @param id
     * @return an EasyCard retrieved from its serial
   */
  public User getUserById(int id) throws NoResultException {
    return (User) entityManager.createQuery(
        "FROM User e WHERE e.id LIKE :id")
        .setParameter("id", id)
        .getSingleResult();
  }
  
  /**
   * Return the users having the passed email.
     * @param id
     * @return an EasyCard retrieved from its serial
   */
  public List<User> getUserSearch(String firstName) throws NoResultException {
    return (List<User>) entityManager.createQuery(
        "FROM User e WHERE e.firstName LIKE CONCAT('%',:firstName,'%')")
        .setParameter("firstName", firstName)
        .getResultList();
  }
  
  
  /**
   * Return the user having the passed email.
     * @param msisdn
     * @return an EasyCard retrieved from its serial
   */
  public User getUserByMsIsdn(String msisdn) throws NoResultException {
    return (User) entityManager.createQuery(
        "FROM User e WHERE e.msisdn LIKE :msisdn")
        .setParameter("msisdn", msisdn)
        .getSingleResult();
  }
  
    /**
   * Return the user having the passed email.
     * @param eMail
     * @return 
   */
  public User getUserByEmail(String eMail) {
    User user;
    try {
        user = (User) entityManager.createQuery(
        "FROM User e WHERE e.email LIKE :eCPwD")
        .setParameter("eCPwD", eMail)
        .getSingleResult();
    }
    catch(javax.persistence.NoResultException exception){
        System.out.println("Il ne trouve rien" + exception.getMessage() );
        return null;
    }

    return user;

  }  


  /**
   * Update the passed user in the database.
     * @param user
   */
  public void update(User user) {
      entityManager.merge(user); 
  }

  // ------------------------
  // PRIVATE FIELDS
  // ------------------------
  
  // An EntityManager will be automatically injected from entityManagerFactory
  // setup on DatabaseConfig class.
  @PersistenceContext
  private EntityManager entityManager;
  
} // class UserDao
