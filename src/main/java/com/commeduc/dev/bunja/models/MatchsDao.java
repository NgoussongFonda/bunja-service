package com.commeduc.dev.bunja.models;

import com.commeduc.dev.bunja.oo.Matchs;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

/**
 * This class is used to access data for the User entity.
 * Repository annotation allows the component scanning support to find and 
 * configure the DAO without any XML configuration and also provide the Spring 
 * exception translation.
 * Since we've setup setPackagesToScan and transaction manager on
 * DatabaseConfig, any bean method annotated with Transactional will cause
 * Spring to magically call begin() and commit() at the start/end of the
 * method. If exception occurs it will also call rollback().
 */
@Repository
@Transactional
public class MatchsDao {
  
  // ------------------------
  // PUBLIC METHODS
  // ------------------------
  
  /**
   * Save the betbattle in the database.
     * @param betevent
   */
  public Matchs create(Matchs betevent) {
    entityManager.persist(betevent);
    return betevent;
  }
  
  /**
   * Delete the betbattle from the database.
     * @param betevent
   */
  public void delete(Matchs betevent) {
    if (entityManager.contains(betevent))
      entityManager.remove(betevent);
    else
      entityManager.remove(entityManager.merge(betevent));
    return;
  }
  
  /**
   * Return all the betbattle stored in the database.
     * @return 
   */
  @SuppressWarnings("unchecked")
  public List<Matchs> getAll() {
    return entityManager.createQuery("from Matchs").getResultList();
  }
  
  /**
   * Return the betbattle having the passed id.
     * @param betevent
     * @return an BetBattle retrieved from its id
   */
  public Matchs getBetBattleById(int id) {
      return entityManager.find(Matchs.class, id);
  }
  
  /**
   * Update the passed betbattle in the database.
     * @param betevent
   */
  public void update(Matchs betevent) {
      entityManager.merge(betevent); 
  }
  
  /**
   * Return the betbattle having the passed id.
     * @param srId
     * @return an BetBattle retrieved from its id
   */
  public Matchs getMatchsBySrId(int srId) {
      
      try {
        return (Matchs)entityManager.createQuery("from Matchs WHERE sr_id = :srId").setParameter("srId", srId).getSingleResult();
      }
      catch(Exception e){}
      
      return null;
  }

  // ------------------------
  // PRIVATE FIELDS
  // ------------------------
  
  // An EntityManager will be automatically injected from entityManagerFactory
  // setup on DatabaseConfig class.
  @PersistenceContext
  private EntityManager entityManager;
  
} // class UserDao
