package com.commeduc.dev.bunja.models;

import com.commeduc.dev.bunja.oo.Team;
import com.commeduc.dev.bunja.oo.Tournament;
import com.commeduc.dev.bunja.oo.TournamentTeam;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

/**
 * This class is used to access data for the User entity.
 * Repository annotation allows the component scanning support to find and 
 * configure the DAO without any XML configuration and also provide the Spring 
 * exception translation.
 * Since we've setup setPackagesToScan and transaction manager on
 * DatabaseConfig, any bean method annotated with Transactional will cause
 * Spring to magically call begin() and commit() at the start/end of the
 * method. If exception occurs it will also call rollback().
 */
@Repository
@Transactional
public class TournamentTeamDao {
  
  // ------------------------
  // PUBLIC METHODS
  // ------------------------
  
  /**
   * Save the betbattle in the database.
     * @param betevent
   */
  public TournamentTeam create(TournamentTeam betevent) {
    entityManager.persist(betevent);
    return betevent;
  }
  
  /**
   * Delete the betbattle from the database.
     * @param betevent
   */
  public void delete(TournamentTeam betevent) {
    if (entityManager.contains(betevent))
      entityManager.remove(betevent);
    else
      entityManager.remove(entityManager.merge(betevent));
    return;
  }
  
  /**
   * Return all the betbattle stored in the database.
     * @return 
   */
  @SuppressWarnings("unchecked")
  public List<TournamentTeam> getAll() {
    return entityManager.createQuery("from TournamentTeam").getResultList();
  }
  
  /**
   * Return the betbattle having the passed id.
     * @param betevent
     * @return an BetBattle retrieved from its id
   */
  public TournamentTeam getTournamentById(int id) {
      return entityManager.find(TournamentTeam.class, id);
  }
  
  /**
   * Update the passed betbattle in the database.
     * @param betevent
   */
  public void update(TournamentTeam betevent) {
      entityManager.merge(betevent); 
  }
  
  
  /**
   * Return the betbattle having the passed id.
     * @param betevent
     * @return an BetBattle retrieved from its id
   */
  public TournamentTeam findTournamentTeam(Tournament tournamentId, Team teamId) {
      
      try {
        return (TournamentTeam)entityManager.createQuery("from TournamentTeam WHERE tournament_id = :tournamentId AND team_id = :teamId")
                .setParameter("tournamentId", tournamentId.getId())
                .setParameter("teamId", teamId.getId()).getSingleResult();
      }
      catch(Exception e){}
      
      return null;
  }

  // ------------------------
  // PRIVATE FIELDS
  // ------------------------
  
  // An EntityManager will be automatically injected from entityManagerFactory
  // setup on DatabaseConfig class.
  @PersistenceContext
  private EntityManager entityManager;
  
} // class UserDao
