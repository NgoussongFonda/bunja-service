package com.commeduc.dev.bunja.oo;

import com.commeduc.dev.bunja.oo.City;
import com.commeduc.dev.bunja.oo.Player;
import com.commeduc.dev.bunja.oo.Team;
import com.commeduc.dev.bunja.oo.User;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-24T11:06:55")
@StaticMetamodel(Country.class)
public class Country_ { 

    public static volatile CollectionAttribute<Country, Player> playerCollection;
    public static volatile CollectionAttribute<Country, Team> teamCollection;
    public static volatile ListAttribute<Country, User> userList;
    public static volatile SingularAttribute<Country, String> countryCode;
    public static volatile SingularAttribute<Country, String> name;
    public static volatile SingularAttribute<Country, Integer> id;
    public static volatile ListAttribute<Country, City> cityList;
    public static volatile SingularAttribute<Country, String> abbreviation;

}