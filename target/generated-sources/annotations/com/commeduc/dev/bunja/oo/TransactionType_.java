package com.commeduc.dev.bunja.oo;

import com.commeduc.dev.bunja.oo.Transactions;
import com.commeduc.dev.bunja.oo.VTransaction;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-24T11:06:55")
@StaticMetamodel(TransactionType.class)
public class TransactionType_ { 

    public static volatile CollectionAttribute<TransactionType, VTransaction> vTransactionCollection;
    public static volatile SingularAttribute<TransactionType, String> name;
    public static volatile CollectionAttribute<TransactionType, Transactions> transactionsCollection;
    public static volatile SingularAttribute<TransactionType, Integer> id;
    public static volatile SingularAttribute<TransactionType, String> abbreviation;

}