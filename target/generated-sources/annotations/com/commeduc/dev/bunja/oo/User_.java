package com.commeduc.dev.bunja.oo;

import com.commeduc.dev.bunja.oo.BetBattleParticipants;
import com.commeduc.dev.bunja.oo.BetEvent;
import com.commeduc.dev.bunja.oo.City;
import com.commeduc.dev.bunja.oo.Country;
import com.commeduc.dev.bunja.oo.Transactions;
import com.commeduc.dev.bunja.oo.VTransaction;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-24T11:06:55")
@StaticMetamodel(User.class)
public class User_ { 

    public static volatile SingularAttribute<User, String> lastName;
    public static volatile SingularAttribute<User, String> address;
    public static volatile CollectionAttribute<User, VTransaction> vTransactionCollection;
    public static volatile CollectionAttribute<User, BetEvent> betEventCollection;
    public static volatile CollectionAttribute<User, Transactions> transactionsCollection;
    public static volatile SingularAttribute<User, City> cityId;
    public static volatile SingularAttribute<User, Date> creationDate;
    public static volatile SingularAttribute<User, Country> countryId;
    public static volatile SingularAttribute<User, String> firstName;
    public static volatile SingularAttribute<User, String> password;
    public static volatile SingularAttribute<User, String> phoneNumber;
    public static volatile SingularAttribute<User, Integer> id;
    public static volatile SingularAttribute<User, String> msisdn;
    public static volatile SingularAttribute<User, String> email;
    public static volatile CollectionAttribute<User, BetBattleParticipants> betBattleParticipantsCollection;

}