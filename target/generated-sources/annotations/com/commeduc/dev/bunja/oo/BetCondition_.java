package com.commeduc.dev.bunja.oo;

import com.commeduc.dev.bunja.oo.BetEvent;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-24T11:06:55")
@StaticMetamodel(BetCondition.class)
public class BetCondition_ { 

    public static volatile SingularAttribute<BetCondition, Integer> occurence;
    public static volatile SingularAttribute<BetCondition, Integer> conditionId;
    public static volatile SingularAttribute<BetCondition, BetEvent> betEventId;
    public static volatile SingularAttribute<BetCondition, Integer> id;
    public static volatile SingularAttribute<BetCondition, Integer> value;

}