package com.commeduc.dev.bunja.oo;

import com.commeduc.dev.bunja.oo.BetBattle;
import com.commeduc.dev.bunja.oo.User;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-24T11:06:55")
@StaticMetamodel(BetBattleParticipants.class)
public class BetBattleParticipants_ { 

    public static volatile SingularAttribute<BetBattleParticipants, Boolean> side;
    public static volatile SingularAttribute<BetBattleParticipants, Boolean> initiator;
    public static volatile SingularAttribute<BetBattleParticipants, Integer> id;
    public static volatile SingularAttribute<BetBattleParticipants, BetBattle> betBattleId;
    public static volatile SingularAttribute<BetBattleParticipants, User> userId;

}