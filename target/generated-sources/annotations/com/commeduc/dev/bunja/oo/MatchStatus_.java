package com.commeduc.dev.bunja.oo;

import com.commeduc.dev.bunja.oo.Matchs;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-24T11:06:55")
@StaticMetamodel(MatchStatus.class)
public class MatchStatus_ { 

    public static volatile CollectionAttribute<MatchStatus, Matchs> matchsCollection;
    public static volatile SingularAttribute<MatchStatus, String> name;
    public static volatile SingularAttribute<MatchStatus, Integer> id;
    public static volatile SingularAttribute<MatchStatus, String> abbreviation;

}