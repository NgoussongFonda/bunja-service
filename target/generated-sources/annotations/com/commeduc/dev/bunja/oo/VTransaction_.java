package com.commeduc.dev.bunja.oo;

import com.commeduc.dev.bunja.oo.TransactionType;
import com.commeduc.dev.bunja.oo.User;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-24T11:06:55")
@StaticMetamodel(VTransaction.class)
public class VTransaction_ { 

    public static volatile SingularAttribute<VTransaction, String> amount;
    public static volatile SingularAttribute<VTransaction, TransactionType> transactionTypeId;
    public static volatile SingularAttribute<VTransaction, Integer> id;
    public static volatile SingularAttribute<VTransaction, User> userId;
    public static volatile SingularAttribute<VTransaction, Date> timestamp;

}