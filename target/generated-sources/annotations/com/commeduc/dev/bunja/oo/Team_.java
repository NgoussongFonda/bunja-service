package com.commeduc.dev.bunja.oo;

import com.commeduc.dev.bunja.oo.Country;
import com.commeduc.dev.bunja.oo.TeamPlayer;
import com.commeduc.dev.bunja.oo.TournamentTeam;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-24T11:06:55")
@StaticMetamodel(Team.class)
public class Team_ { 

    public static volatile CollectionAttribute<Team, TournamentTeam> tournamentTeamCollection;
    public static volatile CollectionAttribute<Team, TeamPlayer> teamPlayerCollection;
    public static volatile SingularAttribute<Team, String> qualifier;
    public static volatile SingularAttribute<Team, String> name;
    public static volatile SingularAttribute<Team, Integer> id;
    public static volatile SingularAttribute<Team, String> abbreviation;
    public static volatile SingularAttribute<Team, Country> countryId;

}