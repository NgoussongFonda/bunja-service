package com.commeduc.dev.bunja.oo;

import com.commeduc.dev.bunja.oo.BetBattle;
import com.commeduc.dev.bunja.oo.Conditions;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-24T11:06:55")
@StaticMetamodel(BetBattleCondition.class)
public class BetBattleCondition_ { 

    public static volatile SingularAttribute<BetBattleCondition, Integer> occurence;
    public static volatile SingularAttribute<BetBattleCondition, Conditions> conditionId;
    public static volatile SingularAttribute<BetBattleCondition, Integer> id;
    public static volatile SingularAttribute<BetBattleCondition, Integer> value;
    public static volatile SingularAttribute<BetBattleCondition, BetBattle> betBattleId;

}