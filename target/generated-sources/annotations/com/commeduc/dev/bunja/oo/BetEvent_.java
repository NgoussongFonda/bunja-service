package com.commeduc.dev.bunja.oo;

import com.commeduc.dev.bunja.oo.BetCondition;
import com.commeduc.dev.bunja.oo.BetType;
import com.commeduc.dev.bunja.oo.Matchs;
import com.commeduc.dev.bunja.oo.User;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-24T11:06:55")
@StaticMetamodel(BetEvent.class)
public class BetEvent_ { 

    public static volatile CollectionAttribute<BetEvent, BetCondition> betConditionCollection;
    public static volatile SingularAttribute<BetEvent, BetType> betTypeId;
    public static volatile SingularAttribute<BetEvent, String> name;
    public static volatile SingularAttribute<BetEvent, Integer> id;
    public static volatile SingularAttribute<BetEvent, User> userId;
    public static volatile SingularAttribute<BetEvent, Matchs> matchId;
    public static volatile SingularAttribute<BetEvent, Date> timestamp;

}