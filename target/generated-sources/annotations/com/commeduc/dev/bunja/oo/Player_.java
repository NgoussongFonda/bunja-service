package com.commeduc.dev.bunja.oo;

import com.commeduc.dev.bunja.oo.Country;
import com.commeduc.dev.bunja.oo.TeamPlayer;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-24T11:06:55")
@StaticMetamodel(Player.class)
public class Player_ { 

    public static volatile SingularAttribute<Player, String> gender;
    public static volatile CollectionAttribute<Player, TeamPlayer> teamPlayerCollection;
    public static volatile SingularAttribute<Player, String> name;
    public static volatile SingularAttribute<Player, Integer> jerseyNumber;
    public static volatile SingularAttribute<Player, Integer> weight;
    public static volatile SingularAttribute<Player, Date> dateOfBirth;
    public static volatile SingularAttribute<Player, Integer> id;
    public static volatile SingularAttribute<Player, Integer> preferredFoot;
    public static volatile SingularAttribute<Player, String> type;
    public static volatile SingularAttribute<Player, Country> countryId;
    public static volatile SingularAttribute<Player, Integer> height;

}