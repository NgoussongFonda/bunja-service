package com.commeduc.dev.bunja.oo;

import com.commeduc.dev.bunja.oo.BetBattleCondition;
import com.commeduc.dev.bunja.oo.BetBattleParticipants;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-24T11:06:55")
@StaticMetamodel(BetBattle.class)
public class BetBattle_ { 

    public static volatile SingularAttribute<BetBattle, Boolean> validated;
    public static volatile CollectionAttribute<BetBattle, BetBattleCondition> betBattleConditionCollection;
    public static volatile SingularAttribute<BetBattle, Integer> id;
    public static volatile SingularAttribute<BetBattle, Integer> matchId;
    public static volatile SingularAttribute<BetBattle, Date> timestamp;
    public static volatile CollectionAttribute<BetBattle, BetBattleParticipants> betBattleParticipantsCollection;

}