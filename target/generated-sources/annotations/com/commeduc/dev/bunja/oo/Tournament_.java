package com.commeduc.dev.bunja.oo;

import com.commeduc.dev.bunja.oo.Sport;
import com.commeduc.dev.bunja.oo.TournamentTeam;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-24T11:06:55")
@StaticMetamodel(Tournament.class)
public class Tournament_ { 

    public static volatile CollectionAttribute<Tournament, TournamentTeam> tournamentTeamCollection;
    public static volatile SingularAttribute<Tournament, Sport> sportId;
    public static volatile SingularAttribute<Tournament, String> name;
    public static volatile SingularAttribute<Tournament, Integer> id;
    public static volatile SingularAttribute<Tournament, String> abbreviation;

}