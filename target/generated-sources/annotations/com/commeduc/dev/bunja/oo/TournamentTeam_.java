package com.commeduc.dev.bunja.oo;

import com.commeduc.dev.bunja.oo.Matchs;
import com.commeduc.dev.bunja.oo.Team;
import com.commeduc.dev.bunja.oo.Tournament;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-24T11:06:55")
@StaticMetamodel(TournamentTeam.class)
public class TournamentTeam_ { 

    public static volatile CollectionAttribute<TournamentTeam, Matchs> matchsCollection;
    public static volatile SingularAttribute<TournamentTeam, Team> teamId;
    public static volatile SingularAttribute<TournamentTeam, Tournament> tournamentId;
    public static volatile SingularAttribute<TournamentTeam, Integer> id;
    public static volatile CollectionAttribute<TournamentTeam, Matchs> matchsCollection1;

}