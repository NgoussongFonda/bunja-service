package com.commeduc.dev.bunja.oo;

import com.commeduc.dev.bunja.oo.Country;
import com.commeduc.dev.bunja.oo.User;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-24T11:06:55")
@StaticMetamodel(City.class)
public class City_ { 

    public static volatile ListAttribute<City, User> userList;
    public static volatile SingularAttribute<City, String> name;
    public static volatile SingularAttribute<City, Integer> id;
    public static volatile SingularAttribute<City, String> abbreviation;
    public static volatile SingularAttribute<City, Country> countryId;

}