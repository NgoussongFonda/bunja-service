package com.commeduc.dev.bunja.oo;

import com.commeduc.dev.bunja.oo.Tournament;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-24T11:06:55")
@StaticMetamodel(Sport.class)
public class Sport_ { 

    public static volatile CollectionAttribute<Sport, Tournament> tournamentCollection;
    public static volatile SingularAttribute<Sport, String> name;
    public static volatile SingularAttribute<Sport, Integer> id;
    public static volatile SingularAttribute<Sport, String> abbreviation;

}