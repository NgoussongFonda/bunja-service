package com.commeduc.dev.bunja.oo;

import com.commeduc.dev.bunja.oo.BetBattleCondition;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-24T11:06:55")
@StaticMetamodel(Conditions.class)
public class Conditions_ { 

    public static volatile CollectionAttribute<Conditions, BetBattleCondition> betBattleConditionCollection;
    public static volatile SingularAttribute<Conditions, String> name;
    public static volatile SingularAttribute<Conditions, Integer> id;
    public static volatile SingularAttribute<Conditions, String> abbreviation;
    public static volatile SingularAttribute<Conditions, Integer> parentCondition;

}