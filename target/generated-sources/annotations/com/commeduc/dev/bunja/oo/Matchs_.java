package com.commeduc.dev.bunja.oo;

import com.commeduc.dev.bunja.oo.BetEvent;
import com.commeduc.dev.bunja.oo.MatchStatus;
import com.commeduc.dev.bunja.oo.TournamentTeam;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-24T11:06:55")
@StaticMetamodel(Matchs.class)
public class Matchs_ { 

    public static volatile SingularAttribute<Matchs, MatchStatus> matchStatusId;
    public static volatile SingularAttribute<Matchs, Date> schedule;
    public static volatile CollectionAttribute<Matchs, BetEvent> betEventCollection;
    public static volatile SingularAttribute<Matchs, String> appreciation;
    public static volatile SingularAttribute<Matchs, Integer> id;
    public static volatile SingularAttribute<Matchs, TournamentTeam> homeTeamId;
    public static volatile SingularAttribute<Matchs, TournamentTeam> visitingTeamId;
    public static volatile SingularAttribute<Matchs, String> status;

}