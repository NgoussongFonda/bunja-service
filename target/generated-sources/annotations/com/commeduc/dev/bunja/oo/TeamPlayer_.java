package com.commeduc.dev.bunja.oo;

import com.commeduc.dev.bunja.oo.Player;
import com.commeduc.dev.bunja.oo.Team;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-24T11:06:55")
@StaticMetamodel(TeamPlayer.class)
public class TeamPlayer_ { 

    public static volatile SingularAttribute<TeamPlayer, Team> teamId;
    public static volatile SingularAttribute<TeamPlayer, Boolean> active;
    public static volatile SingularAttribute<TeamPlayer, Integer> id;
    public static volatile SingularAttribute<TeamPlayer, Player> playerId;

}