package com.commeduc.dev.bunja.oo;

import com.commeduc.dev.bunja.oo.BetEvent;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-24T11:06:55")
@StaticMetamodel(BetType.class)
public class BetType_ { 

    public static volatile CollectionAttribute<BetType, BetEvent> betEventCollection;
    public static volatile SingularAttribute<BetType, String> name;
    public static volatile SingularAttribute<BetType, Integer> id;

}