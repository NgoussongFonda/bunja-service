package com.commeduc.dev.bunja.models;

import com.commeduc.dev.bunja.oo.Player;
import com.commeduc.dev.bunja.oo.Sport;
import com.commeduc.dev.bunja.oo.Team;
import com.commeduc.dev.bunja.oo.TeamPlayer;
import com.commeduc.dev.bunja.oo.Tournament;
import com.commeduc.dev.bunja.oo.TournamentTeam;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

/**
 * This class is used to access data for the User entity.
 * Repository annotation allows the component scanning support to find and 
 * configure the DAO without any XML configuration and also provide the Spring 
 * exception translation.
 * Since we've setup setPackagesToScan and transaction manager on
 * DatabaseConfig, any bean method annotated with Transactional will cause
 * Spring to magically call begin() and commit() at the start/end of the
 * method. If exception occurs it will also call rollback().
 */
@Repository
@Transactional
public class TeamPlayerDao {
  
  // ------------------------
  // PUBLIC METHODS
  // ------------------------
  
  /**
   * Save the betbattle in the database.
     * @param betevent
   */
  public TeamPlayer create(TeamPlayer betevent) {
    entityManager.persist(betevent);
    return betevent;
  }
  
  /**
   * Delete the betbattle from the database.
     * @param betevent
   */
  public void delete(TeamPlayer betevent) {
    if (entityManager.contains(betevent))
      entityManager.remove(betevent);
    else
      entityManager.remove(entityManager.merge(betevent));
    return;
  }
  
  /**
   * Return all the betbattle stored in the database.
     * @return 
   */
  @SuppressWarnings("unchecked")
  public List<TeamPlayer> getAll() {
    return entityManager.createQuery("from TeamPlayer").getResultList();
  }
  
  /**
   * Return the betbattle having the passed id.
     * @param betevent
     * @return an BetBattle retrieved from its id
   */
  public TeamPlayer getTeamPlayerById(int id) {
      return entityManager.find(TeamPlayer.class, id);
  }
  
  /**
   * Update the passed betbattle in the database.
     * @param betevent
   */
  public void update(TeamPlayer betevent) {
      entityManager.merge(betevent); 
  }
  
  
  /**
   * Return the betbattle having the passed id.
     * @param betevent
     * @return an BetBattle retrieved from its id
   */
  public TeamPlayer getTeamPlayer(Player playerId, Team teamId) {
      
      try {
        return (TeamPlayer)entityManager.createQuery("from Player WHERE sr_id = :playerId AND team_id = :teamId")
                .setParameter("playerId", playerId.getId())
                .setParameter("teamId", teamId.getId()).getSingleResult();
      }
      catch(Exception e){}
      
      return null;
  }

  // ------------------------
  // PRIVATE FIELDS
  // ------------------------
  
  // An EntityManager will be automatically injected from entityManagerFactory
  // setup on DatabaseConfig class.
  @PersistenceContext
  private EntityManager entityManager;
  
} // class UserDao
